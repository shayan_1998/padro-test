import React from "react";
import TaskContext from "../../context/TaskContext";
import { useContext, useState } from "react";
import TextInput from "../ui/TextInput";
import TextArea from "../ui/TextArea";
import Button from "../ui/Button";
import Plus from "../../assets/svg/plus.svg";
import validation from "../../utils/validation";
import { toast } from "react-toastify";

function CreateForm() {
	const context = useContext(TaskContext);
	const [form, setForm] = useState({
		title: "",
		description: "",
	});
	const [errors, setErrors] = useState({});

	function onInputChange(name, value) {
		setForm((prevFrom) => ({ ...prevFrom, [name]: value }));
	}

	function submitForm(event) {
		event.preventDefault();
		let { valid, errors } = validation(form);
		if (!valid) {
			setErrors(errors);
			return;
		}
		setErrors({});

		let task = {
			...form,
			id: context.tasks.length + 1,
			status: "ToDo",
		};
		context.updateTasks([...context.tasks, task]);
		toast("New Task Created", { type: "success" });
		setForm({
			title: "",
			description: "",
		});
	}

	return (
		<form onSubmit={submitForm} className="form">
			<h3 className="pageTitle">Add a new Task</h3>
			<TextInput
				label="Title"
				name="title"
				defaultValue={form.title}
				onChangeValue={onInputChange}
				error={errors.title}
			/>
			<TextArea
				label="Description"
				name="description"
				defaultValue={form.description}
				onChangeValue={onInputChange}
				error={errors.description}
			/>
			<Button type="submit">
				<img src={Plus} alt="plus" />
				Add
			</Button>
		</form>
	);
}

export default CreateForm;
