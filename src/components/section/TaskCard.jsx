import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Edit from "../../assets/svg/edit.svg";

function TaskCard({ task }) {
	return (
		<div className="card">
			<div className="taskCard">
				<div className="title">{task.title}</div>
				<div className="desc">{task.description}</div>
				<div className="row">
					<div className="status">{task.status}</div>
					<Link to={`/edit/${task.id}`}>
						<img src={Edit} alt="Edit" />
					</Link>
				</div>
			</div>
		</div>
	);
}

TaskCard.propTypes = {
	task: PropTypes.object,
};

TaskCard.defaultProps = {
	task: {},
};

export default TaskCard;
