import React from "react";
import TaskContext from "../../context/TaskContext";
import { useContext } from "react";
import TaskCard from "./TaskCard";

function TasksList() {
	const context = useContext(TaskContext);
	let tasks = [...context.tasks].reverse();

	return (
		<div className="taskList">
			<div className="head">Tasks</div>
			<div className={`wrapper ${!tasks.length ? "empty" : ""}`}>
				{tasks.map((task, index) => (
					<TaskCard key={index} task={task} />
				))}
				{tasks.length === 0 && (
					<div className="emptyList">
						<div>You have nothing to do.</div>
						<div>Go get some sleep.</div>
					</div>
				)}
			</div>
		</div>
	);
}

export default TasksList;
