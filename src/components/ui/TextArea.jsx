import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

function TextArea({
	defaultValue,
	name,
	label,
	onChangeValue,
	numOfLine,
	type,
	error,
}) {
	const [value, setValue] = useState(defaultValue);
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (defaultValue) {
			onFocus();
		} else {
			setValue("");
		}
	}, [defaultValue]);

	function onChange(event) {
		setValue(event.target.value);
		onChangeValue(name, event.target.value);
	}

	function onFocus() {
		setIsActive(true);
	}
	function onBlur() {
		if (value === "") {
			setIsActive(false);
		}
	}

	return (
		<div className="textInput">
			<div className="wrapper">
				<div className={`label ${isActive ? "active" : ""}`}>
					{label}
				</div>
				<textarea
					rows={numOfLine}
					type={type}
					onFocus={onFocus}
					onBlur={onBlur}
					value={defaultValue}
					onChange={onChange}
				></textarea>
			</div>
			{error && <div className="error">{error}</div>}
		</div>
	);
}

TextArea.propTypes = {
	defaultValue: PropTypes.string,
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	type: PropTypes.string,
	error: PropTypes.string,
	numOfLine: PropTypes.number,
	onChangeValue: PropTypes.func.isRequired,
};

TextArea.defaultProps = {
	defaultValue: "",
	label: "",
	name: "",
	type: "text",
	error: "",
	numOfLine: 5,
	onChangeValue: () => {},
};

export default TextArea;
