import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

function TextInput({ defaultValue, name, label, onChangeValue, type, error }) {
	const [value, setValue] = useState(defaultValue);
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		if (defaultValue) {
			setValue(defaultValue);
			onFocus();
		} else {
			setValue("");
		}
	}, [defaultValue]);

	function onChange(event) {
		setValue(event.target.value);
		onChangeValue(name, event.target.value);
	}

	function onFocus() {
		setIsActive(true);
	}
	function onBlur() {
		if (value === "") {
			setIsActive(false);
		}
	}

	return (
		<div className="textInput">
			<div className="wrapper">
				<div className={`label ${isActive ? "active" : ""}`}>
					{label}
				</div>
				<input
					type={type}
					onFocus={onFocus}
					onBlur={onBlur}
					value={value}
					onChange={onChange}
				/>
			</div>
			{error && <div className="error">{error}</div>}
		</div>
	);
}

TextInput.propTypes = {
	defaultValue: PropTypes.string,
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	type: PropTypes.string,
	error: PropTypes.string,
	onChangeValue: PropTypes.func.isRequired,
};

TextInput.defaultProps = {
	defaultValue: "",
	label: "",
	name: "",
	type: "text",
	error: "",
	onChangeValue: () => {},
};

export default TextInput;
