import React from "react";
import PropTypes from "prop-types";

function Button({ children, type, outline, onClick }) {
	return (
		<button
			className={`btn ${outline ? "outline" : ""}`}
			type={type}
			onClick={onClick}
		>
			{children}
		</button>
	);
}

Button.propTypes = {
	type: PropTypes.string,
	outline: PropTypes.any,
	onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
	type: "button",
	outline: null,
	onClick: () => {},
};

export default Button;
