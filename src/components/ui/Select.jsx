import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

function Select({ name, label, onChangeValue, options, defaultValue, error }) {
	const [value, setValue] = useState("");
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		onFocus();
	});

	function onChange(event) {
		setValue(event.target.value);
		onChangeValue(name, event.target.value);
	}

	function onFocus() {
		setIsActive(true);
	}
	function onBlur() {
		if (value === "") {
			setIsActive(false);
		}
	}

	return (
		<div className="textInput">
			<div className="wrapper">
				<div className={`label ${isActive ? "active" : ""}`}>
					{label}
				</div>
				<select
					onFocus={onFocus}
					onBlur={onBlur}
					onChange={onChange}
					value={defaultValue}
				>
					<option value="" disabled>
						Select an option
					</option>
					{options.map((item) => (
						<option key={item.id} value={item.id}>
							{item.label}
						</option>
					))}
				</select>
				<img
					src={require("../../assets/svg/triangle.svg").default}
					alt="triangle-down"
					className="triangle"
				/>
			</div>
			{error && <div className="error">{error}</div>}
		</div>
	);
}

Select.propTypes = {
	label: PropTypes.string.isRequired,
	name: PropTypes.string.isRequired,
	options: PropTypes.array.isRequired,
	defaultValue: PropTypes.string,
	error: PropTypes.string,
	onChangeValue: PropTypes.func.isRequired,
};

Select.defaultProps = {
	label: "",
	name: "",
	defaultValue: "",
	options: [],
	error: "",
	onChangeValue: () => {},
};

export default Select;
