import React from "react";

function Nav({ children }) {
	return <nav>Task Manager > {children}</nav>;
}

export default Nav;
