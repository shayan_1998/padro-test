import React from "react";
import Nav from "../components/base/Nav";
import PropTypes from "prop-types";

function AppLayout({ children, name }) {
	return (
		<div className="layout">
			<Nav>{name}</Nav>
			{children}
		</div>
	);
}
AppLayout.propTypes = {
	name: PropTypes.string.isRequired,
};

AppLayout.defaultProps = {
	name: "",
};

export default AppLayout;
