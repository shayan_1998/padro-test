import { BrowserRouter } from "react-router-dom";
import Router from "./router";
import TaskContext from "./context/TaskContext";
import "./styles/App.scss";
import { useState } from "react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
	const [tasks, setTasks] = useState([]);
	function updateTasks(list) {
		setTasks(list);
	}
	const ProviderValue = {
		tasks,
		updateTasks,
	};

	return (
		<TaskContext.Provider value={ProviderValue}>
			<BrowserRouter>
				<Router />
				<ToastContainer />
			</BrowserRouter>
		</TaskContext.Provider>
	);
}

export default App;
