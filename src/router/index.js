import * as React from "react";
import { Routes, Route } from "react-router-dom";
import Tasks from "../pages/tasks";
import TasksEdit from "../pages/tasks/Edit";
import NotFound from "../pages/errors/NotFound";

export default function Router() {
	return (
		<Routes>
			<Route path="/" element={<Tasks />}></Route>
			<Route path="/edit/:taskId" element={<TasksEdit />}></Route>
			<Route path="*" element={<NotFound />} />
		</Routes>
	);
}
