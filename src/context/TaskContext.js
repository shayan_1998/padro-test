import React from "react";

const TaskContext = React.createContext({
	tasks: [],
	updateTasks: () => {},
});
//each item of tasks array => {id:'',title:'',description:'',status:''}

export default TaskContext;
