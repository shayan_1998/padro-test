import React from "react";
import AppLayout from "../../layouts/AppLayout";
import CreateForm from "../../components/section/CreateForm";
import TasksList from "../../components/section/TasksList";

function Tasks() {
	return (
		<AppLayout name="Home">
			<div className="pageWrapper">
				<CreateForm />
				<TasksList />
			</div>
		</AppLayout>
	);
}

export default Tasks;
