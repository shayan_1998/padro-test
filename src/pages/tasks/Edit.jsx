import React from "react";
import TaskContext from "../../context/TaskContext";
import { useContext, useState, useEffect } from "react";
import TextInput from "../../components/ui/TextInput";
import TextArea from "../../components/ui/TextArea";
import Select from "../../components/ui/Select";
import Button from "../../components/ui/Button";
import AppLayout from "../../layouts/AppLayout";
import { useParams, useNavigate } from "react-router-dom";
import EditLight from "../../assets/svg/edit-light.svg";
import { validStatusList } from "../../utils/TaskStatus";
import validation from "../../utils/validation";
import { toast } from "react-toastify";

function TasksEdit() {
	const context = useContext(TaskContext);
	let { taskId } = useParams();
	let navigate = useNavigate();
	const [form, setForm] = useState({
		id: "",
		title: "",
		description: "",
		status: "",
	});
	const [errors, setErrors] = useState({});
	const [selectOptions, setSelectOptions] = useState([]);

	useEffect(() => {
		let editTask = context.tasks.find((task) => task.id == taskId);
		if (!editTask) {
			navigate("/");
			return;
		}
		setForm(editTask);
		setSelectOptions(validStatusList(editTask.status));
	}, [taskId]);

	function onInputChange(name, value) {
		setForm((prevFrom) => ({ ...prevFrom, [name]: value }));
	}

	function submitForm(event) {
		event.preventDefault();
		let { valid, errors } = validation(form);
		if (!valid) {
			setErrors(errors);
			return;
		}
		setErrors({});
		let tasks = context.tasks.map((task) => {
			return task.id === form.id ? form : task;
		});
		context.updateTasks(tasks);
		setSelectOptions(validStatusList(form.status));
		toast("Task updated successfully", { type: "success" });
	}

	function goBack() {
		navigate("/");
	}

	return (
		<AppLayout name="Edit">
			<form onSubmit={submitForm} className="form edit">
				<h3 className="pageTitle">Edit Task</h3>
				<TextInput
					label="Title"
					name="title"
					defaultValue={form.title}
					onChangeValue={onInputChange}
					error={errors.title}
				/>
				<TextArea
					label="Description"
					name="description"
					numOfLine={15}
					defaultValue={form.description}
					onChangeValue={onInputChange}
					error={errors.description}
				/>
				<Select
					label="Status"
					name="status"
					options={selectOptions}
					defaultValue={form.status}
					onChangeValue={onInputChange}
					error={errors.status}
				/>
				<div className="row">
					<Button type="submit">
						<img src={EditLight} alt="Edit" />
						Edit
					</Button>
					<div className="spacer"></div>
					<Button outline onClick={goBack}>
						Cancel
					</Button>
				</div>
			</form>
		</AppLayout>
	);
}

export default TasksEdit;
