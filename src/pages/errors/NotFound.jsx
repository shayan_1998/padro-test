import React from "react";
import { Link } from "react-router-dom";
import AppLayout from "../../layouts/AppLayout";

function NotFound() {
	return (
		<AppLayout name="Home">
			<div className="notFound">
				<div className="bigText">404</div>
				<h1>Page Not Found</h1>
				<Link to="/">back to home</Link>
			</div>
		</AppLayout>
	);
}

export default NotFound;
