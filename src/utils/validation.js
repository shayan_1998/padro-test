const validation = (form) => {
	let errors = {};
	let valid = true;
	for (const key in form) {
		if (form[key] == "") {
			errors = {
				...errors,
				[key]: `${key} is required`,
			};
			valid = false;
		}
	}

	return { errors, valid };
};

export default validation;
