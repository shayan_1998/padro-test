const statusList = [
	{ id: "ToDo", label: "ToDo" },
	{ id: "InProgress", label: "InProgress" },
	{ id: "Blocked", label: "Blocked" },
	{ id: "InQA", label: "InQA" },
	{ id: "Done", label: "Done" },
	{ id: "Deployed", label: "Deployed" },
];

const validStatusList = (status) => {
	let list = [];
	switch (status) {
		case "ToDo":
			list = [
				{ id: "ToDo", label: "ToDo" },
				{ id: "InProgress", label: "InProgress" },
			];
			break;
		case "InProgress":
			list = [
				{ id: "InProgress", label: "InProgress" },
				{ id: "Blocked", label: "Blocked" },
				{ id: "InQA", label: "InQA" },
			];
			break;
		case "Blocked":
			list = [
				{ id: "Blocked", label: "Blocked" },
				{ id: "ToDo", label: "ToDo" },
			];
			break;
		case "InQA":
			list = [
				{ id: "InQA", label: "InQA" },
				{ id: "ToDo", label: "ToDo" },
				{ id: "Done", label: "Done" },
			];
			break;
		case "Done":
			list = [
				{ id: "Done", label: "Done" },
				{ id: "Deployed", label: "Deployed" },
			];
			break;
		case "Deployed":
			list = [{ id: "Deployed", label: "Deployed" }];
			break;

		default:
			list = statusList;
			break;
	}

	return list;
};

export { statusList, validStatusList };
